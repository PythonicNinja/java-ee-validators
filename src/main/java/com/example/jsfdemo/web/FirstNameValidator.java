package com.example.jsfdemo.web;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import com.example.jsfdemo.domain.Person;
import com.example.jsfdemo.service.PersonManager;

@FacesValidator("firstNameValidator")
public class FirstNameValidator implements Validator {
	
	@Inject
	private PersonManager pm;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		
		String firstName = (String) value;

		for (Person person : pm.getAllPersons()) {
			if (person.getFirstName().equalsIgnoreCase(firstName)) {
				FacesMessage message = new FacesMessage();
				message.setDetail("Person with this Name already exists in database");
				message.setSummary("Person with this Name already exists in database");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(message);
			}
		}
	}
}