package com.example.jsfdemo.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SessionScoped
@Named("hobbiesBean")
public class HobbiesBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> Hobbies = new ArrayList<String>(){
		private static final long serialVersionUID = 1L;
	{
	    add("skiing");
	    add("running");
	    add("swimming");
	    add("learning JEE");
	    add("walking");
	    add("programming");
	}};

	
	public List<String> getHobbies() {
		return Hobbies;
	}
	
}
