package com.example.jsfdemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.example.jsfdemo.domain.Person;

@ApplicationScoped
public class PersonManager {
	private List<Person> db = new ArrayList<Person>();

	public void addPerson(Person person) {
		Person newPerson = new Person();

		newPerson.setFirstName(person.getFirstName());
		newPerson.setZipCode(person.getZipCode());
		newPerson.setPin(person.getPin());
		newPerson.setDateOfBirth(person.getDateOfBirth());
		newPerson.setMarried(person.isMarried());
		newPerson.setWeight(person.getWeight());
		newPerson.setDateOfRetirement(person.getDateOfRetirement());
		newPerson.setFavColor(person.getFavColor());
		newPerson.setHobby(person.getHobby());
		newPerson.setPesel(person.getPesel());

		db.add(newPerson);
	}

	// Removes the person with given PIN
	public void deletePerson(Person person) {
		Person personToRemove = null;
		for (Person p : db) {
			if (person.getPin().equals(p.getPin())) {
				personToRemove = p;
				break;
			}
		}
		if (personToRemove != null)
			db.remove(personToRemove);
	}
	
	// Returns person with given firstName
	public Person getPerson(String firstName) {
    	
		Person personToReturn = null;
		for (Person p : db) {
			if (firstName.equals(p.getFirstName())) {
				personToReturn = p;
				break;
			}
		}
		return personToReturn;
	}

	public List<Person> getAllPersons() {
		return db;
	}
}
